public class Schueler {

	// Anfang Attribute
	private String name;
	private int joker;
	private int blamiert;
	private int fragen;
	private boolean istAnwesend;
	private String vorname;
	// private String fullName;
	
	// Ende Attribute

	// Konstruktor
	public Schueler(String name, int joker, int blamiert, int fragen, String vorname) {
		this.name = name;
		this.joker = joker;
		this.blamiert = blamiert;
		this.fragen = fragen;
		this.istAnwesend = true;
		this.vorname = vorname;
		// this.fullName = fullName;
	}

	// Anfang Methoden
	public String getName() {
		return name;
	}

	
	public boolean getIstAnwesend() {
		return this.istAnwesend;
	}	

	public int getJoker() {
		return joker;
	}

	public int getBlamiert() {
		return blamiert;
	}

	public int getFragen() {
		return fragen;
	}

	public void blamiert() {
		this.blamiert++;
	}

	public void gefragt() {
		this.fragen++;
	}

	public void nichtDa() {
		this.fragen--;
		this.istAnwesend = false;
	}

	public String getVorname() {
		return vorname;
	}
	
	public void jokerEingesetzt() {
		this.joker++;
	}
	// Ende Methoden
}
