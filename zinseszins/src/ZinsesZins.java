import java.util.Scanner;

public class ZinsesZins {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Laufzeit des Sparvertrags (in Jahren) : ");
		int jahre = tastatur.nextInt();
		
		System.out.print("Wie hoch ist da Kapital (in Euro) das Sie anlegen wollen: ");
		double kapital = tastatur.nextDouble();
		
		System.out.print("Zinssatz: ");
		double zinssatz = tastatur.nextDouble();
		
		int laufzeit = 1;
		double kapitalsteigung = kapital;
		
		while(laufzeit <= jahre) {
			kapitalsteigung = kapitalsteigung + ((kapitalsteigung * zinssatz) / 100);
			laufzeit++;
			
		}
		System.out.printf("Eingezahltes Kapital: %.2f Euro\n", kapital);
		System.out.printf("Ausgezahltes Kapital: %.2f Euro", kapitalsteigung);
		
	}

}
