package uhu;
import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		double[] arraypreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		String[] fahrkarten = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"}; 

		// Bestellung//
		// ----------
		while (true) {
			double zuZahlenderBetrag = fahrkartenbestellungErfassen(arraypreise, fahrkarten);

			// Geldeinwurf
			// -----------
			double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();

			// R�ckgeldberechnung
			// ------------------
			rueckgeldAusgeben(rueckgabebetrag);

			zuZahlenderBetrag = 0;
			rueckgabebetrag = 0;
		}
	}

	public static double fahrkartenbestellungErfassen(double[] arraypreise, String[] fahrkarten) {

		Scanner tastatur = new Scanner(System.in);
		int anzahlTickets = 0;
		int ticketAuswahl;
		double ticketPreis = 0.00000;
		double zuZahlenderBetrag;

		System.out.println("W�hlen Sie Ihre Wunschfahrkarte\n"
				+ "1	"+ fahrkarten[0]+ "		"+			arraypreise[0]+"�\n"
				+ "2	"+ fahrkarten[1]+ "		"+			arraypreise[1]+"�\n"
				+ "3	"+ fahrkarten[2]+ "		"+			arraypreise[2]+"�\n"
				+ "4	"+ fahrkarten[3]+ "				"+	arraypreise[3]+"�\n"
				+ "5	"+ fahrkarten[4]+ "			"+		arraypreise[4]+"�\n"
				+ "6	"+ fahrkarten[5]+ "			"+		arraypreise[5]+"�\n"
				+ "7	"+ fahrkarten[6]+ "			"+		arraypreise[6]+"�\n"
				+ "8	"+ fahrkarten[7]+ "	"+				arraypreise[7]+"�\n"
				+ "9	"+ fahrkarten[8]+ "	"+				arraypreise[8]+"�\n"
				+ "10	"+ fahrkarten[9]+ "	"+				arraypreise[9]+"�\n");

		boolean standardausgabe = false;
		while(standardausgabe == false) {

			System.out.print("Ihre Wahl: ");
			ticketAuswahl = tastatur.nextInt();

			switch(ticketAuswahl) {
			case 1:
				System.out.println("Sie haben "+ fahrkarten[0] +" ausgew�hlt, der Preis betr�gt 2,90�");
				ticketPreis = arraypreise[0];
				standardausgabe = true;
				break;

			case 2:
				System.out.println("Sie haben "+ fahrkarten[1] +" ausgew�hlt, der Preis betr�gt 3,30�");
				ticketPreis = arraypreise[1];
				standardausgabe = true;
				break;

			case 3:
				System.out.println("Sie haben "+ fahrkarten[2] +" ausgew�hlt, der Preis betr�gt 3,60�");
				ticketPreis = arraypreise[2];
				standardausgabe = true;
				break;
			case 4:
				System.out.println("Sie haben "+ fahrkarten[3] +" ausgew�hlt, der Preis betr�gt 1,90�");
				ticketPreis = arraypreise[3];
				standardausgabe = true;
				break;
			case 5:
				System.out.println("Sie haben "+ fahrkarten[4] +" ausgew�hlt, der Preis betr�gt 8,60�");
				ticketPreis = arraypreise[4];
				standardausgabe = true;
				break;
			case 6:
				System.out.println("Sie haben "+ fahrkarten[5] +" ausgew�hlt, der Preis betr�gt 9,00�");
				ticketPreis = arraypreise[5];
				standardausgabe = true;
				break;
			case 7:
				System.out.println("Sie haben "+ fahrkarten[6] +" ausgew�hlt, der Preis betr�gt 9,60�");
				ticketPreis = arraypreise[6];
				standardausgabe = true;
				break;
			case 8:
				System.out.println("Sie haben "+ fahrkarten[7] +" ausgew�hlt, der Preis betr�gt 23,50�");
				ticketPreis = arraypreise[7];
				standardausgabe = true;
				break;
			case 9:
				System.out.println("Sie haben "+ fahrkarten[8] +" ausgew�hlt, der Preis betr�gt 24,30�");
				ticketPreis = arraypreise[8];
				standardausgabe = true;
				break;
			case 10:
				System.out.println("Sie haben "+ fahrkarten[9] +" ausgew�hlt, der Preis betr�gt 24,90�");
				ticketPreis = arraypreise[9];
				standardausgabe = true;
				break;

			default:
				System.out.print("  >>falsche Eingabe<<\n\n");
				break;
			}

		}
		System.out.print("Wie viele Tickets m�chten Sie?: ");
		anzahlTickets = tastatur.nextInt();

		zuZahlenderBetrag = ticketPreis * anzahlTickets;

		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		double rueckgabebetrag;

		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {

			System.out.format("Noch zu zahlen: %4.2f � %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));

			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();

			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		return rueckgabebetrag;

	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {

			System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.49) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.19) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.09) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.04)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.\n");

	}
}