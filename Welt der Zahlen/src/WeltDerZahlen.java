/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
        String anzahlSterne = "10^9 bis 20^9";
    
    // Wie viele Einwohner hat Berlin?
        String bewohnerBerlin = "3,7*10^6";
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      int alterTage = 9694;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
      int  gewichtKilogramm =   190000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       String flaecheGroessteLand = "17^*10^6";
    
    // Wie groß ist das kleinste Land der Erde?
    
       String flaecheKleinsteLand = "O,44";
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Einwohner Berlin: " + bewohnerBerlin);
    System.out.println("Mein Alter in Tagen: " + alterTage);
    System.out.println("Gewicht schwerstes Tier: " + gewichtKilogramm);
    System.out.println("Größtes Land der Welt: " + flaecheGroessteLand);
    System.out.println("Kleinstes Land der Welt: " + flaecheKleinsteLand);
   
    
  }
}

